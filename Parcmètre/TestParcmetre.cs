﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcmètre
{
    using NUnit.Framework;

    class TestParcmetre
    {
        private Parcmetre _parcmetre;

        [SetUp]
        public void Setup()
        {
            _parcmetre = new Parcmetre();
        }

        [TestCase(10)]
        [TestCase(20)]
        [TestCase(50)]
        [TestCase(100)]
        [TestCase(200)]
        public void AddOneCoin_AcceptCoin(int cents)
        {
            _parcmetre.AddCoin(cents);
        }


        [TestCase(5)]
        [TestCase(21)]
        public void AddOneCoin_RejectCoin(int cents)
        {
            Assert.Throws<InvalidOperationException>(() => _parcmetre.AddCoin(cents));
        }

        [TestCase(10, 0)]
        [TestCase(20, 0)]
        [TestCase(50, 0)]
        [TestCase(100, 20)]
        [TestCase(200, 60)]
        public void AddOneCoin_IncreaseTime(int cents, int minutes)
        {
            _parcmetre.AddCoin(cents);
            Assert.AreEqual(minutes, _parcmetre.Minutes);
        }

        [TestCase(10)]
        [TestCase(20)]
        [TestCase(50)]
        [TestCase(100)]
        [TestCase(200)]
        public void AddCoin_IncreaseTotal(int cents)
        {
            _parcmetre.AddCoin(cents);
            Assert.AreEqual(cents, _parcmetre.Total);
        }

        [TestCase(10, 10, 20)]
        [TestCase(20, 50, 70)]
        public void AddTwoCoins_IncreaseTotal(int first, int second, int total)
        {
            _parcmetre.AddCoin(first);
            _parcmetre.AddCoin(second);
            Assert.AreEqual(total, _parcmetre.Total);
        }

        [TestCase(0, 100, 20)]
        [TestCase(10, 100, 20)]
        [TestCase(100, 10, 20)]
        [TestCase(100, 100, 60)]
        [TestCase(200, 100, 120)]
        [TestCase(300, 200, 60 * 12)]
        [TestCase(600, 200, 60 * 24)]
        [TestCase(300, 100, 2*60 + 20)]
        public void AddMultipleCoins_IncreaseTimeForOverallAccount(int initialAmount, int cents, int minutes)
        {
            for (var i = 0; i < initialAmount / 10; i++)
                _parcmetre.AddCoin(10);

            _parcmetre.AddCoin(cents);
            Assert.AreEqual(minutes, _parcmetre.Minutes);
        }

        [TestCase(100, 20)]
        [TestCase(200, 60)]
        [TestCase(50, 0)]
        public void AddCoin_GetEndDate(int cents, int minutesToAdd)
        {
            DateTime previousEndDate = _parcmetre.EndDate;
            _parcmetre.AddCoin(cents);
            
            Assert.AreEqual(previousEndDate.AddMinutes(minutesToAdd), _parcmetre.EndDate);
        }

        public void AddMultipleEuros_IncreaseMinutes(int numberOfEuros, int minutes)
        {
            
        }
    }
}
