﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcmètre
{
    class Parcmetre
    {
        private HashSet<int> _coins = new HashSet<int> { 10, 20, 50, 100, 200 };

        public int Minutes { get; set; }

        public int Total { get; set; }

        private DateTime FirstCoinEndDate { get; } = DateTime.Now;

        public DateTime EndDate { get; private set; }

        public bool IsAllowed(int cents)
        {
            return _coins.Contains(cents);
        }

        public Parcmetre()
        {
            EndDate = FirstCoinEndDate;
        }

        public void AddCoin(int cents)
        {
            if (!IsAllowed(cents))
                throw new InvalidOperationException();

            Total += cents;
            int remaining = Total;
            Minutes = 0;

            while (true)
            {
                if (remaining>= 800)
                {
                    Minutes += 60 * 24;
                    remaining -= 800;
                }
                else if (remaining >= 500)
                {
                    Minutes += 60 * 12;
                    remaining -= 500;
                }
                else if (remaining >= 300)
                {
                    Minutes += 120;
                    remaining -= 300;
                }
                else if (remaining >= 200)
                {
                    Minutes += 60;
                    remaining -= 200;
                }
                else if (remaining >= 100)
                {
                    Minutes += 20;
                    remaining -= 100;
                }
                else
                {
                    break;
                }
            }

            EndDate = FirstCoinEndDate.AddMinutes(Minutes);
        }
    }
}
